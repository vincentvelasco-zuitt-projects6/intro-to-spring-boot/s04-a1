package com.velascob142.s02.s02app.controllers;



import com.velascob142.s02.s02app.models.User;
import com.velascob142.s02.s02app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;


    @RequestMapping(value="/users", method= RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User newUser){
        userService.createUser(newUser);
        return new ResponseEntity<>("New user was create", HttpStatus.CREATED);

    }




    @RequestMapping(value="/users", method=RequestMethod.GET)
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }


    @RequestMapping(value="/users/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long id, @RequestBody User updatedUser){
        userService.updateUser(id, updatedUser);
        return new ResponseEntity<>("User was updated", HttpStatus.OK);

    }

    @RequestMapping(value="/users/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
        return new ResponseEntity<>("User was deleted", HttpStatus.OK);
    }
}
